const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}

const array = Object.entries(users);

// // // users who are interested in video games

let videoGamePeople = array.filter((person) => {
    let hobbies = person[1].interests.toString();
    if (hobbies.includes("Video Games")) {
        return true;
    }
});

console.log(Object.fromEntries(videoGamePeople));

// users in germany

let usersFromGermany = array.filter((person) => {
    return (person[1].nationality === "Germany");
});

console.log(Object.fromEntries(usersFromGermany));

// users who have masters degree

let mastersUsers = array.filter((person) => {
    return (person[1].qualification === "Masters");
});

console.log(Object.fromEntries(mastersUsers));

// users grouped according to their programming languages

let programs = array.reduce((accumulator, current) => {
    let languages = current[1].desgination.toString().replace('Senior', '').replace('Developer', '').replace('Intern', '').replace('-', '').trim();
    accumulator[current[0]] = languages;
    return accumulator;
}, {});

console.log(programs);

// sort the users according to their age 

let ageSort = array.sort((first, second) => {
    if (first[1].age > second[1].age) {
        return -1;
    } else {
        return 1;
    }
});

console.log(Object.fromEntries(ageSort));

// sort the users according to their seniority level

let desginationSort = array.reduce((accumulator, current) => {
    let temp = current[1].desgination.toString();
    if (temp.includes("Senior") && temp.includes("Developer")) {
        accumulator[0].push(current);
    } else if (temp.includes("Developer")) {
        accumulator[1].push(current);
    } else {
        accumulator[2].push(current);
    }
    return accumulator;
}, [[],[],[]]);

console.log(Object.fromEntries(desginationSort.flat()));

